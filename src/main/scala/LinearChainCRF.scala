import java.io.{File, FileOutputStream, PrintWriter}

import cc.factorie.Factorie.{CategoricalDomain, LabeledCategoricalVariable}
import cc.factorie.app.nlp.{Document, Sentence, Token}
import cc.factorie.infer.{InferByBPChain, MaximizeByBPChain}
import cc.factorie.model.{DotTemplateWithStatistics1, DotTemplateWithStatistics2, Parameters, TemplateModel, WeightsSet}
import cc.factorie.variable._
import cc.factorie.la
import cc.factorie.la.{DenseTensor2, Tensor2}
import cc.factorie.optimize._

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.io.Source
import scala.pickling.binary.BinaryPickle
import scala.pickling.io.TextFileOutput

object LinearChainCRF {

  // Define the random variable for the label
  object LabelDomain extends CategoricalDomain[String]
  class Label(label: String, _token : Token) extends LabeledCategoricalVariable(label) {
    def domain = LabelDomain
    val token = _token
  }

  // For the token, we will use the library-provided class Token, but we need to add the features associated with this class separately using the attr method
  object TokenFeaturesDomain extends CategoricalVectorDomain[String]
  class TokenFeatures(_token: Token) extends BinaryFeatureVectorVariable[String] {
    val token = _token
    def domain = TokenFeaturesDomain
  }

  // Define the template model that contains 2 template factors, one for the transition variables (label, label) and
  // the other for observation variables (label, token)
  val model = new TemplateModel with Parameters

  // Factor between label and observed token
  val t1 = new DotTemplateWithStatistics2[Label, TokenFeatures] {
    val weights = model.Weights(new la.DenseTensor2(LabelDomain.size, TokenFeaturesDomain.dimensionSize))
    def unroll1(label: Label) = Factor(label, label.token.attr[TokenFeatures])
    def unroll2(tf: TokenFeatures) = Factor(tf.token.attr[Label], tf)
  }
  // Transition factors between two successive labels
  val t2 = new DotTemplateWithStatistics2[Label, Label] {
    val weights = model.Weights(new la.DenseTensor2(LabelDomain.size, LabelDomain.size))
    def unroll1(label: Label) = if (label.token.hasPrev) Factor(label.token.prev.attr[Label], label) else Nil
    def unroll2(label: Label) = if (label.token.hasNext) Factor(label, label.token.next.attr[Label]) else Nil
  }

  // Add the 2 template factors to the model
  model.addTemplates(t1, t2)


  // Load the documents from the training and testing datasets
  def loadDocuments(fileName: String): Seq[Document] = {
    val documents = new ArrayBuffer[Document]
    var document = new Document().setName(documents.length.toString)
    var sentence = new Sentence(document)
    val source = Source.fromFile(new File(fileName))

    for (line <- source.getLines()) {
      // If the line is empty, then it is the end of the sentence
      if (line.length < 2) {
        if (sentence.nonEmpty) {
          sentence = new Sentence(document)
        }
      }
      // Start of a new document
      else if (line.trim.contains("-DOCSTART-")) {
        if (document.tokenCount > 0) {
          documents += document
          document = new Document().setName(documents.length.toString)
          sentence = new Sentence(document)
        }
      }
      else {
        val lineSplit = line.split(' ')
        val word = lineSplit(0)
        val pos = lineSplit(1)
        val label = lineSplit(3)
        val token = new Token(sentence, word)
        token.attr += new Label(label, token)
        // Build all the features by just looking at the current token at this point
        val features = buildLocalFeatures(word, pos, token)
        token.attr += features
      }
    }
    if (document.tokenCount > 0)
      documents += document
    documents
  }

  val Capitalized = "^[A-Z].*".r
  val Numeric = "^[0-9]+$".r
  val Punctuation = "[-,\\.;:?!()]+".r

  def buildLocalFeatures(word: String, pos: String, token: Token): TokenFeatures = {
    val features = new TokenFeatures(token)
    features += "W=" + word
    features += "POS=" + pos
    features += "SHAPE=" + cc.factorie.app.strings.stringShape(word, 2)
    if (word.length > 3) features += "PRE=" + word.substring(0, 3)
    if (Capitalized.findFirstMatchIn(word) != None) features += "CAPITALIZED"
    if (Numeric.findFirstMatchIn(word) != None) features += "NUMERIC"
    if (Punctuation.findFirstMatchIn(word) != None) features += "PUNCTUATION"
    features
  }

  def precision(str: String, labels : Seq[(String, String)]) : Double = {
    val strLabels = labels.filter(label => label._1.equals(str))
    if (strLabels.length == 0)
      return 0.0
    val correctCount = strLabels.map(label => if (label._1.equals(label._2)) 1 else 0).sum
    val assignedCount = strLabels.length
    correctCount.toDouble / assignedCount.toDouble
  }

  def recall(str: String, labels: Seq[(String, String)]): Double = {
    val strTargetLabels = labels.filter(label => label._2.equals(str))
    if (strTargetLabels.length == 0)
      return 0.0
    val correctCount = strTargetLabels.map(label => if (label._1.equals(label._2)) 1 else 0).sum
    val targetCount = strTargetLabels.length
    correctCount.toDouble / targetCount.toDouble
  }

  // Method that computes the F1 score of all labels and returns it as a hasmap with key being the label and the value being the F1 score
  def computeF1(seq: Seq[Seq[LinearChainCRF.Label]]) : mutable.HashMap[String, Double] = {
    // Create a new sequence of tuple containing the actual and the target label, with the prefixes (I-, B-) removed
    val labels = seq.flatten.map(label => {
      var actual = ""
      if (label.categoryValue.startsWith("I-") || label.categoryValue.startsWith("B-"))
        actual = label.categoryValue.substring(2)
      else
        actual = label.categoryValue
      var target = ""
      if (label.target.categoryValue.startsWith("I-") || label.target.categoryValue.startsWith("B-"))
        target = label.target.categoryValue.substring(2)
      else
        target = label.target.categoryValue
      (actual, target)
    })
    val map = mutable.HashMap[String, Double]()
    for (label <- List("PER", "LOC", "ORG", "MISC", "O")) {
      val p = precision(label, labels)
      val r = recall(label, labels)
      val f1 = if ((p + r) == 0.0) 0.0 else ((2 * p * r)/(p + r))
      map += (label -> f1)
    }
    map
  }


  def main(args: Array[String]): Unit = {

    if (args.length != 2)
      throw new Error("Usage: LinearChainCRF trainfile testfile")

    // Load the training and testing documents
    val trainFile: String = args(0)
    val testFile: String = args(1)
    val trainDocs = loadDocuments(trainFile)

    // Add contextual features to the current token.
    // Here, the context is w-1, w-2, w-3, w-4, w+1, w+2, w+3, w+4
    trainDocs.foreach(doc => doc.tokens.foreach(t => {
      if (t.hasPrev)
      t.attr[TokenFeatures] ++= t.prev.attr[TokenFeatures].activeCategories.filter(!_.contains("@")).map(c => c + "@-1")
      if (t.hasPrev(2))
        t.attr[TokenFeatures] ++= t.prev(2).attr[TokenFeatures].activeCategories.filter(!_.contains("@")).map(c => c + "@-2")
      if (t.hasPrev(3))
        t.attr[TokenFeatures] ++= t.prev(3).attr[TokenFeatures].activeCategories.filter(!_.contains("@")).map(c => c + "@-3")
      if (t.hasPrev(4))
        t.attr[TokenFeatures] ++= t.prev(4).attr[TokenFeatures].activeCategories.filter(!_.contains("@")).map(c => c + "@-4")
      if (t.hasNext)
        t.attr[TokenFeatures] ++= t.next.attr[TokenFeatures].activeCategories.filter(!_.contains("@")).map(c => c + "@+1")
      if (t.hasNext(2))
        t.attr[TokenFeatures] ++= t.next(2).attr[TokenFeatures].activeCategories.filter(!_.contains("@")).map(c => c + "@+2")
      if (t.hasNext(3))
        t.attr[TokenFeatures] ++= t.next(3).attr[TokenFeatures].activeCategories.filter(!_.contains("@")).map(c => c + "@+3")
      if (t.hasNext(4))
        t.attr[TokenFeatures] ++= t.next(4).attr[TokenFeatures].activeCategories.filter(!_.contains("@")).map(c => c + "@+4")
    }))

    // Create the log-likelihood examples from the training docs to generate the parameter gradients
    val trainLabelsSentences: Seq[Seq[Label]] = trainDocs.map(_.tokens.toSeq.map(_.attr[Label]))
    val examples = trainLabelsSentences.map(s => new LikelihoodExample(s, model, InferByBPChain))

    // Using LBFGS for parameter learning
    val optimizer = new LBFGS with L2Regularization
    optimizer.variance = 100.0

    println("Starting training")
    val startTime = System.currentTimeMillis
    // Now perform the training using the examples and the optimizer above
//    implicit val random = new scala.util.Random(0)
    val trainer = new ParallelBatchTrainer(model.parameters, optimizer)
    trainer.trainFromExamples(examples)
    println("Total training time: " + (System.currentTimeMillis - startTime)/1000 + " seconds")

    // Before loading the testing docs, freeze the label and token features domains
    LabelDomain.freeze()
    TokenFeaturesDomain.freeze()
    println("All labels from the training data: ")
    LabelDomain.categories.foreach(c => println(c))
    val testDocs = loadDocuments(testFile)

    implicit val random = new scala.util.Random(0)
    val testLabelsSentences = testDocs.map(_.tokens.toSeq.map(_.attr[Label]))
    testLabelsSentences.foreach {
      variables => {
        val sampler = cc.factorie.infer.InferByGibbsSampling
        sampler.infer(variables, model)
      }
//      variables => cc.factorie.infer.BP.inferChainMax(variables, model).setToMaximize(null)
    }

    // Once the inference is done, compute the F1 score
    val f1 = computeF1(testLabelsSentences)
    for (key <-  f1.keys) {
      println("Key: " + key + ", F1: " + f1.apply(key))
    }

    println("test token accuracy=" + HammingObjective.accuracy(testLabelsSentences.flatten))
    System.exit(0)

  }
}
